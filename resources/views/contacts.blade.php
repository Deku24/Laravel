@extends('template')

@section('content')

<h1>Contact us</h1>

@if(session()->has('info'))
<h3>{{ session('info') }}</h3>
@else
<form method="POST" action="contact">
 <p><label for="name">
 Name
 <input type="text" name="name" value="{{ old('name') }}">
 {!! $errors->first('name','<span class=error>:message</span>') !!}
 </label><p>
 
 <p><label for="email">
 Email
 <input type="email" name="email" value="{{ old('email') }}">
 </label><p>
 
 <p><label for="message">
 Message
 <textarea name="message"></textarea>
 </label><p>
 <input type="submit" value="Send">
</form>
@endif

@stop