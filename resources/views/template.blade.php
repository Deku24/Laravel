<!doctype html>
<html>
	<head
		<meta charset="utf-8">
		<style>
		.active{
			text-decoration: none;
			color: green;
		}

		.error{
			color: red;
			font-size:  12px;
		}
		</style>
		<title>greetings</title>
	</head>
	<body>
	<?php  
		function active($url){
			return   request()->is($url) ? 'active' : '';   
		}
	?>
		<h1>welcome to the hunger games</h1>
		<header>
			<nav>
				<ul>
					<li><a 
					class="{{ active('/') }}"
					href="{{ route('home')}}">Home</a></li>
					<li><a 
					class="{{ active('greetings/*')}}"
					href="{{ route('greetings', 'name')}} ">Greetings</a></li>
					<li><a
					class="{{ active('contact')}}"
					 href="{{ route('contact') }}">Contact</a></li>
				</ul>
			</nav>
		</header>
		@yield('content')
		<footer>
			<h3>@ I'm using php, blade and laravel </h3>
		</footer>
	</body>
</html>