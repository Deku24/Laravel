<?php


Route::get('/', ['as'=> 'home', 'uses'=> 'PagesController@home']);
 
 
Route::get('contact',[ 'as' =>'contact', 'uses'=>'PagesController@contact'
]);
 
Route::post('contact','PagesController@messages');

Route::get('greetings/{name?}', ['as'=>'greetings','uses'=>'PagesController@greetings'
])->where('name',"[A-Za-z]+");

