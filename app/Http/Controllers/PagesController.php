<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\formValidation;
 

class PagesController extends Controller
{
    // one way to inject the class Request to the class
   /* protected $request;

    public function __construct(Request $request){
        $this->request = $request;
    }
    */

    public function home(){

        return view('home');
    }
    
    public function contact (){

        return view('contacts');
    }

    public function greetings ($name= "guest"){

        return view('greetings', compact('name'));
    }


    public function messages(formValidation $request){
    $data= $request->all();// process the data of the form
    
    return redirect()->route('contact')->with('info', 'your message has been sent successfully');

}


}
